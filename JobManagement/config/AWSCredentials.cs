﻿namespace JobManagement.config
{
    public class AWSCredentials
    {
        public string AccessKey { get; set; } = "";
        public string SecretKey { get; set; } = "";
    }
}
