﻿using Amazon.Runtime;
using JobManagement.Models;

namespace JobManagement.Services
{
    public interface IStorageService
    {
        Task<S3ResponseDto> UploadFileAsync(string name, MemoryStream inputStream, string bucketName);
        Task<S3ResponseDto> DownloadFileAsync(string name, MemoryStream inputStream, string bucketName);
        Task<IFormFile> GetContentFile(
           string name,
           string bucketName);
        string getFullPathFile(string filePath);
    }
}
