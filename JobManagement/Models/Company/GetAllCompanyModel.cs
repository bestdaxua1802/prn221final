﻿
using Repository.Utils;

namespace JobManagement.Models.Company
{
    public class GetAllCompanyModel
    {
        public string? KeyWords { get; set; }
        public int PageSize { get; set; } = Constant.DEFAULT_PAGE_SIZE;
        public int PageNumber { get; set; } = Constant.DEFAULT_PAGE_NUMBER;
    }
}
