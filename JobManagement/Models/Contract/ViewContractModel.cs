﻿using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Contract
{
    public class ViewContractModel
    {
        [Required]
        public long CompanyId { get; set; }
        [Required]
        public long ResumeId { get; set; }
        [Required]
        public long JobDescriptionId { get; set; }
    }
}
