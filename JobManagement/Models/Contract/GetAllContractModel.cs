﻿
using Repository.Utils;

namespace JobManagement.Models.Contract
{
    public class GetAllContractModel
    {
        public string? KeyWords { get; set; }
        public long? CompanyId { get; set; }
        public int PageSize { get; set; } = Constant.DEFAULT_PAGE_SIZE;
        public int PageNumber { get; set; } = Constant.DEFAULT_PAGE_NUMBER;
    }
}
