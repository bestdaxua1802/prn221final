﻿using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Contract
{
    public class UpdateContractModel
    {
        [Key]
        public long Id { get; set; }
        public long? CompanyId { get; set; }

        public string CompanyName { get; set; }


        public long? ResumeId { get; set; }

        public string ResumeName { get; set; }

        [Required]
        public DateTime InterviewTime { get; set; }

        public string? Interviewer { get; set; }

        [Required]
        [Range(1, Int64.MaxValue, ErrorMessage = "OfferSalary should be greater than 0")]
        public Double OfferSalary { get; set; }

        [Required]
        [Range(1, Int64.MaxValue, ErrorMessage = "RequestSalary should be greater than 0")]
        public Double RequestSalary { get; set; }

        public long JobDescriptionId { get; set; }

        public string? Description { get; set; }
    }
}
