﻿using BusinessObject.Models;
using Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.JobDescription
{
    public class CreateJobDescriptionModel
    {
        [Required]
        public long CompanyId { get; set; }
    
        [Required]
        [CapitalName(ErrorMessage = "First Title Name Must Be Capitalize")]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Range(1, Int64.MaxValue, ErrorMessage = "RequiredNumber should be greater than 0")]
        public int? RequiredNumber { get; set; }

        [Range(1, Int64.MaxValue, ErrorMessage = "MinSalary should be greater than 0")]
        public long? MinSalary { get; set; }

        [Range(1, Int64.MaxValue, ErrorMessage = "MaxSalary should be greater than 0")]
        public long? MaxSalary { get; set; }

        [Required]
        public JobType Type { get; set; }

        [Required]
        public JobLevel Level { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ClosedAt { get; set; }

        [Required]
        public List<long> SkillIds { get; set; } = new List<long>();
    }
}
