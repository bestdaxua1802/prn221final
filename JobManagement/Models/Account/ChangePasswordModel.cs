﻿

using JobManagement.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Auth
{
    public class ChangePasswordModel
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        [PassWord(ErrorMessage = "Password must between 8-18 characters and has at least 1 uppercase letter ," +
         "1 number,1 digit and 1 special character")]
        public string NewPassword { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
    }
}
