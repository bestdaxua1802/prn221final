﻿

using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Auth
{
    public class InactivateAccountModel
    {
        public long Id { get; set; }
    }
}
