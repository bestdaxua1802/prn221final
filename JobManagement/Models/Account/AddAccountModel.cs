﻿using BusinessObject.Models;
using JobManagement.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Auth
{
    public class AddAccountModel
    {
        [Required]
        [CapitalName(ErrorMessage = "First Letter Name Must Be Capitalize")]
        public string FullName { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        [PassWord(ErrorMessage = "Password must between 8-18 characters and has at least 1 uppercase letter ," +
         "1 number,1 digit and 1 special character")]
        public string Password { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        [PhoneNumberValid(ErrorMessage = "Accepts only 10 digits number, no more no less")]
        public string Phone { get; set; }

        public string? Description { get; set; }

    }
}
