﻿using Applications.Models;
using BusinessObject.Models;
using Domain.Enums;
using JobManagement.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Profile
{
    public class UpdateAccountModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        [CapitalName(ErrorMessage = "First Letter Name Must Be Capitalize")]
        public string FullName { get; set; }
        [Required]
        public string Username { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        [PhoneNumberValid(ErrorMessage = "Accepts only 10 digits number, no more no less")]
        public string Phone { get; set; }
        public string? Description { get; set; }

        public AccountStatus Status { get; set; }

        public bool? IsRootAdmin { get; set; } = false;

        public UpdateAccountModel()
        {

        }

        public UpdateAccountModel(AccountModel account)
        {
            Id = account.Id;
            FullName = account.FullName;
            Username = account.Username;
            Phone = account.Phone;
            Status = account.Status;
            IsRootAdmin = account.IsRootAdmin;
            Description = account.Description;
            Email = account.Email;
        }
    }
}
