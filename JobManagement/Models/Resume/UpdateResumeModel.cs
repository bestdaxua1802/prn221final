﻿
using BusinessObject.Models;
using JobManagement.ModelValidation;
using System.ComponentModel.DataAnnotations;

namespace JobManagement.Models.Resume
{
    public class UpdateResumeModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        [CapitalName(ErrorMessage = "First Letter Name Must Be Capitalize")]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        [PhoneNumberValid(ErrorMessage = "Accepts only 10 digits number, no more no less")]
        public string Phone { get; set; }
        [Required]
        [Birthday(ErrorMessage = "Invalid BirthDay,must be from 1900 to now")]
        public DateTime Birthday { get; set; }
        public string? FileName { get; set; }
        public string? FileUrl { get; set; }
        public IFormFile? UpdatedFile { get; set; }

        [MaxLength(160, ErrorMessage = "Description is not over 160 characters")]
        public string? Description { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "Must choosing at least one skill")]
        public List<long> SkillIds { get; set; } = new List<long>();
    }
}
