﻿using JobManagement.Filters;
using JobManagement.Models.Auth;
using JobManagement.Utils;
using Microsoft.AspNetCore.Mvc;
using Domain.Enums;
using Domain.Entities;
using Applications.Repositories;
using Applications.Models;
using Applications.Services;
using Applications.Interfaces;

namespace JobApplicationManagement.Controllers
{
    [TypeFilter(typeof(AuthorizationFilter))]
    public class AccountController : Controller
    {
        private IAccountRepository _accountRepository;
        private readonly IAccountService _accountService;

        public AccountController(IAccountRepository accountRepository, IAccountService accountService)
        {
            _accountRepository = accountRepository;
            _accountService = accountService;
        }

        public IActionResult Index()
        {
            //List<Account> accounts = _accountService.GetAccounts().ToList();
            return View();
        }

        [HttpGet]
        [Route("Account/AccountDetail/{id}")]
        public IActionResult AccountDetail(long id)
        {
            Account account = _accountService.GetByIdEntites(id);
            if (account == null)
            {
                return RedirectToAction(nameof(Index));
            }
            AccountModel accountModel = new AccountModel()
            {
                Id = account.Id,
                Description = account.Description,
                FullName = account.FullName,
                Username = account.Username,
                Email = account.Email,
                Phone = account.Phone,
                Status = account.Status,
                IsRootAdmin = account.IsRootAdmin
            };


            return View(accountModel);
        }

        public IActionResult Inactivate(InactivateAccountModel model)
        {
            long id = long.Parse(HttpContext.Session.GetString("currentId"));
            if (id == null  || Object.Equals(model.Id, id))
            {
                TempData["Error"] = "Cannot inactivate your self";
                return RedirectToAction(nameof(Index));
            }
            Account account = _accountService.GetByIdEntites(model.Id);
            
            if (account != null)
            {
                if (account.IsRootAdmin != null && (bool)account.IsRootAdmin)
                {
                    TempData["Error"] = "Cannot inactivate root admin";
                    return RedirectToAction(nameof(Index));
                }
                account.Status = AccountStatus.INACTIVE;
                _accountRepository.Update(account);
                _accountRepository.Save();
            }
            return RedirectToAction(nameof(Index));
        }


        public IActionResult Activate(ActivateAccountModel model)
        {
            Account account = _accountService.GetByIdEntites(model.Id);
            if (account != null)
            {
                account.Status = AccountStatus.ACTIVE;
                _accountRepository.Update(account);
                _accountRepository.Save();
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult AddAccount()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(AddAccountModel model)
        {
            if (!Object.Equals(model.Password, model.ConfirmPassword))
            {
                ModelState.TryAddModelError("ConfirmPassword", "Password and Confirm Password must be match");
            }
            if (!ModelState.IsValid)
            {
                return View(nameof(AddAccount), model);
            }
            Account? account = _accountService.GetByUsernameLog(model.Username);
            if (account != null)
            {
                TempData["Error"] = "Existed Username";
                return View(nameof(AddAccount), model);
            }
            account = new Account()
            {
                Username = model.Username,
                Password = HashUtil.GetMD5(model.Password),
                Email = model.Email,
                FullName = model.FullName,
                Phone = model.Phone,
                IsRootAdmin = false,
                Description = model.Description,
                CreatedAt = DateTime.Now,
                Status = AccountStatus.ACTIVE,
            };
            _accountService.CreateAccount(account);
            TempData["Success"] = "Add successfully";
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public PartialViewResult GetAccounts(GetAccountsModel? model)
        {
            var accountPageResult = _accountService.GetAccountPagin(model.Query, model.Status, model.PageNumber, model.PageSize);

            var accounts = accountPageResult.Items;

            ViewData["PageNumber"] = accountPageResult.PageIndex;
            ViewData["TotalPages"] = accountPageResult.TotalPagesCount;
            
            return PartialView("_AccountListView", accounts);
        }
    }
}
