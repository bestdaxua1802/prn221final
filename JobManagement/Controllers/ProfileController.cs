﻿using Applications.Interfaces;
using Applications.Models;
using Domain.Entities;
using JobManagement.Models.Auth;
using JobManagement.Models.Profile;
using JobManagement.Utils;
using Microsoft.AspNetCore.Mvc;

namespace JobManagement.Controllers
{
    public class ProfileController : Controller
    {
        private readonly IAccountService _accountService;

        public ProfileController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult MyAccount()
        {
            string? id = HttpContext.Session.GetString("currentId");
            if (id == null)
            {
                TempData["Error"] = "Account not found";

            }
            AccountModel account = _accountService.GetById(long.Parse(id));
            if (account == null)
            {
                return RedirectToAction(nameof(Index));
            }
            
            return View(account);
        }

        [HttpPost]
        public IActionResult UpdateMyAccount(UpdateAccountModel model)
        {
            if (!ModelState.IsValid)
            {
                string? id = HttpContext.Session.GetString("currentId");
                AccountModel ProfileModel = _accountService.GetById(long.Parse(id));
                if (ProfileModel == null)
                {
                    return RedirectToAction(nameof(Index));
                }
                return View(nameof(MyAccount), ProfileModel);
            }
            AccountModel? account = _accountService.GetByUsername(model.Username);
            if (account == null)
            {
                TempData["Error"] = "Account not found!";
                return RedirectToAction(nameof(MyAccount));
            }
            if (!String.IsNullOrEmpty(model.Email))
            {
                account.Email = model.Email;
            }
            if (!String.IsNullOrEmpty(model.FullName))
            {
                account.FullName = model.FullName;
            }
            if (!String.IsNullOrEmpty(model.Phone))
            {
                account.Phone = model.Phone;
            }
            if (!String.IsNullOrEmpty(model.Description))
            {
                account.Description = model.Description;
            }
            _accountService.Update(account);

            if (Object.Equals(account.Id.ToString(), HttpContext.Session.GetString("currentId")))
            {
                HttpContext.Session.SetString("currentName", account.FullName);
            }

            TempData["Success"] = "Update successfully";
            return RedirectToAction(nameof(MyAccount));
        }

        [HttpGet]
        public IActionResult ChangeMyPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangeMyPassword(ChangePasswordModel model)
        {
            if (!ModelState.IsValid || !Object.Equals(model.NewPassword, model.ConfirmPassword))
            {
                TempData["Error"] = "New Password and Confirm Password must be match";
                return View(nameof(ChangeMyPassword));
            }
            string? id = HttpContext?.Session?.GetString("currentId");

            AccountModel? account = _accountService.GetById(long.Parse(id));
            if (account == null)
            {
                return View(nameof(ChangeMyPassword));
            }

            if (!HashUtil.IsValid(model.OldPassword, account?.Password))
            {

                TempData["Error"] = "Old Password incorrect";
                return View(nameof(ChangeMyPassword));
            }

            await _accountService.UpdatePassword(account.Id, HashUtil.GetMD5(model.NewPassword));
            TempData["Success"] = "Update Successfully";
            return View(nameof(ChangeMyPassword));
        }

    }
}
