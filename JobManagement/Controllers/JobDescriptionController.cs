﻿using Applications.Interfaces;
using Domain.Entities;
using Domain.Enums;
using JobManagement.Models.JobDescription;
using Microsoft.AspNetCore.Mvc;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Drawing.Printing;
using Applications.Models;

namespace JobManagement.Controllers
{
    public class JobDescriptionController : Controller
    {
        private readonly ISkillService _skillService;
        private readonly IResumeService _resumeService;
        private readonly ICompanyService _companyServices;
        private readonly IJobDescriptionService _jobDescriptionService;

        public JobDescriptionController(ISkillService skillService, ICompanyService companyServices, IJobDescriptionService jobDescriptionService, IResumeService resumeService)
        {
            _resumeService = resumeService; 
            _skillService = skillService;
            _companyServices = companyServices;
            _jobDescriptionService = jobDescriptionService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var skills = await _skillService.GetSkills();
            var companies = await _companyServices.GetAll();

            ViewData["Skills"] = skills;
            ViewData["Companies"] = companies;

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Delete(long id)
        {
            _jobDescriptionService.Delete(id);
            TempData["Success"] = "Delete successfully";
            return RedirectToAction(nameof(Index));
        }

        public async Task<PartialViewResult> SearchJobDescriptions(GetAllJobDescriptionModel model)
        {
            var jobsPageResult = await _jobDescriptionService
               .GetAll(model.PageSize,
                        model.PageNumber,
                        model.Query,
                        model.SkillId,
                        model.CompanyId,
                        model.Level,
                        model.Type, false);

            var items = jobsPageResult.Items;

            ViewData["PageNumber"] = jobsPageResult.PageIndex;
            ViewData["TotalPages"] = jobsPageResult.TotalPagesCount;

            return PartialView("_JobListView", items);
        }

        [HttpGet]
        public async Task<PartialViewResult> GetJobDescription(long id)
        {
            var jobDetail = await _jobDescriptionService.GetById(id);
            return PartialView("_JobDetailView", jobDetail);
        }


        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var skills = await _skillService.GetSkills();
            var companies = await _companyServices.GetAll();

            ViewData["Skills"] = skills;
            ViewData["Companies"] = companies;

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateJobDescriptionModel model)
        {

            if (model.MinSalary != null && model.MaxSalary != null && model.MaxSalary <= model.MinSalary)
            {
                ModelState.AddModelError("MaxSalary", "Max Salary must be greater than Min Salary");
            }

            if (model.SkillIds?.Count == 0)
            {
                ModelState.AddModelError("SkillIds", "Required at least one skill");
            }

            DateTime today = DateTime.Today;
            if (model.ClosedAt != null && DateTime.Compare((DateTime)model.ClosedAt, today) < 0)
            {
                ModelState.AddModelError("ClosedAt", "Closed Date must be greater than today");
            }

            if (!ModelState.IsValid)
            {

                ViewData["Skills"] = await _skillService.GetSkills();
                ViewData["Companies"] = await _companyServices.GetAll();
                return View(model);
            }

            CompanyModel company = await _companyServices.GetById(model.CompanyId);
            if (company == null)
            {
                ModelState.AddModelError("CompanyId", "Company Not found");
                return View(model);
            }

            var skills = new List<SkillModel>();
            foreach (var skillId in model.SkillIds)
            {
                SkillModel skill = await _skillService.GetById(skillId);
                if (skill == null)
                {
                    ModelState.AddModelError("SkillIds", "Skill not found");
                    return View(model);
                }
                skills.Add(skill);
            }

            var jobDescription = new JobDescriptionModel
            {
                Title = model.Title.Trim(),
                Description = model.Description.Trim(),
                RequiredNumber = model.RequiredNumber,
                MinSalary = model.MinSalary,
                MaxSalary = model.MaxSalary,
                Type = model.Type,
                Level = model.Level,
                Skills = skills,
                ClosedAt = model.ClosedAt,
                Company = company
            };
            try
            {
                await _jobDescriptionService.Create(jobDescription);
                TempData["Success"] = "Create job description successfully!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Create job description failed. Try again!";
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Update(long id)
        {
            var jobDescription = await _jobDescriptionService.GetById(id);
            if (jobDescription == null)
            {
                return NotFound();
            }
            ViewData["Skills"] = await _skillService.GetSkills();
            ViewData["Companies"] = await _companyServices.GetAll();

            UpdateJobDescriptionModel model = new UpdateJobDescriptionModel
            {
                Id = id,
                Title = jobDescription.Title,
                Description = jobDescription.Description,
                RequiredNumber = jobDescription.RequiredNumber,
                MinSalary = jobDescription.MinSalary,
                MaxSalary = jobDescription.MaxSalary,
                Type = jobDescription.Type,
                Level = jobDescription.Level,
                ClosedAt = jobDescription.ClosedAt,
                CompanyId = jobDescription.CompanyId,
                SkillIds = jobDescription.Skills.Select(skill => skill.Id).ToList()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Update(UpdateJobDescriptionModel model)
        {
            if (model.MinSalary != null && model.MaxSalary != null && model.MaxSalary <= model.MinSalary)
            {
                ModelState.AddModelError("MaxSalary", "Max Salary must be greater than Min Salary");
            }

            if (model.SkillIds?.Count == 0)
            {
                ModelState.AddModelError("SkillIds", "Required at least one skill");
            }

            DateTime today = DateTime.Today;
            if (model.ClosedAt != null && DateTime.Compare((DateTime)model.ClosedAt, today) < 0)
            {
                ModelState.AddModelError("ClosedAt", "Closed Date must be greater than today");
            }

            if (!ModelState.IsValid)
            {
                ViewData["Skills"] = await _skillService.GetSkills();
                ViewData["Companies"] = await _companyServices.GetAll();
                return View(model);
            }
            // Update data
            var jobDescription = await _jobDescriptionService.GetById(model.Id);
            if (jobDescription == null)
            {
                return NotFound();
            }

            var company = await _companyServices.GetById(model.CompanyId);
            if (company == null)
            {
                ModelState.AddModelError("CompanyId", "Company Not found");
                return View(model);
            }

            var skills = new List<SkillModel>();
            foreach (var skillId in model.SkillIds)
            {
                var skill = await _skillService.GetById(skillId);
                if (skill == null)
                {
                    ModelState.AddModelError("SkillIds", "Skill not found");
                    return View(model);
                }
                skills.Add(skill);
            }

            jobDescription.Title = model.Title.Trim();
            jobDescription.Description = model.Description.Trim();
            jobDescription.RequiredNumber = model.RequiredNumber;
            jobDescription.MinSalary = model.MinSalary;
            jobDescription.MaxSalary = model.MaxSalary;
            jobDescription.Type = model.Type;
            jobDescription.Level = model.Level;
            jobDescription.Skills = skills;
            jobDescription.ClosedAt = model.ClosedAt;
            jobDescription.Company = company;

            try
            {
                await _jobDescriptionService.Update(jobDescription);
                TempData["Success"] = "Update job description successfully!";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                TempData["Error"] = "Update job description failed!";
            }

            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> ViewDetail(long id)
        {
            var job = await _jobDescriptionService.GetById(id);
            if (job == null)
            {
                return NotFound();
            }

            List<long> skillIds = job.Skills.Select(skill => skill.Id).ToList();
            List<ResumeModel> resumes = await _resumeService.GetResumes(skillIds);

            ViewData["Resumes"] = resumes.Where(res => job.Resumes.FirstOrDefault(r => r.Id == res.Id) == null)
                .ToList();


            return View(job);
        }

        [HttpGet]
        public async Task<IActionResult> ChooseResume(long id, long resumeId)
        {
            var job = await _jobDescriptionService.GetById(id);
            if (job == null)
            {
                return NotFound();
            }
            var resume = _resumeService.GetResume(resumeId);
            if (resume == null)
            {
                return NotFound();
            }

            if (job.Resumes.FirstOrDefault(res => res.Id == resumeId) != null)
            {
                TempData["Error"] = "Existed resume.";
            }
            else
            {
                await _jobDescriptionService.AddResume(id, resumeId);
                TempData["Success"] = "Choose resume successfully";
            }

            return RedirectToAction(nameof(ViewDetail), new { id });
        }

        [HttpGet]
        public async Task<IActionResult> RemoveResume(long id, long resumeId)
        {
            var job = await _jobDescriptionService.GetById(id);
            if (job == null)
            {
                return NotFound();
            }
            var resume = _resumeService.GetResume(resumeId);
            if (resume == null)
            {
                return NotFound();
            }

            if (job.Resumes.FirstOrDefault(res => res.Id == resumeId) == null)
            {
                TempData["Error"] = "UnExisted resume.";
            }
            else
            {
                await _jobDescriptionService.RemoveResume(id, resumeId);
                TempData["Success"] = "Remove resume from job description successfully";
            }

            return RedirectToAction(nameof(ViewDetail), new { id });
        }


    }
}
