﻿using JobManagement.Models.Auth;
using JobManagement.Utils;
using Microsoft.AspNetCore.Mvc;
using Domain.Enums;
using Domain.Entities;
using Applications.Repositories;
using System.Text;
using Applications.Services;
using Applications.Interfaces;

namespace JobApplicationManagement.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAccountService _accountService;

        public AuthController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Index), model);
            }
            var a = model.Username;
            Account? account = _accountService.GetByUsernameLog(model.Username);
            if (account == null || !HashUtil.IsValid(model.Password, account.Password) || !Object.Equals(account.Status, AccountStatus.ACTIVE))
            {
                ViewBag.Error = "Incorrect Username Password";
                return View(nameof(Index));
            }
            HttpContext.Session.SetString("currentId", account.Id.ToString());
            HttpContext.Session.SetString("currentIsRootAdmin", account.IsRootAdmin + "");
            HttpContext.Session.SetString("currentName", account.FullName);
            HttpContext.Session.SetString("currentUsername", account.FullName);
            string? redirectUrl = HttpContext.Session.GetString("redirectUrl");
            if (redirectUrl != null)
            {
                HttpContext.Session.Remove("redirectUrl");
                return Redirect(redirectUrl);

            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Logout()
        {
            if (!ModelState.IsValid)
            {
                return View(nameof(Index));
            }
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Auth");
        }

    }
}
