﻿using Applications.Interfaces;
using Applications.Services;
using Domain.Entities;
using JobManagement.Models.Company;
using JobManagement.Models.Resume;
using Microsoft.AspNetCore.Mvc;
using static Amazon.S3.Util.S3EventNotification;

namespace JobManagement.Controllers
{
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        public IActionResult Index()
        {
            var companies = _companyService.GetAll();
            return View(companies);
        }

        [HttpGet]
        public IActionResult ViewDetailCompany(long id)
        {
            var company = _companyService.GetCompany(id);
            if (company == null)
            {
                return NotFound();
            }
            return View(company);
        }

        [HttpGet]
        public IActionResult ViewCompanies(GetAllCompanyModel model)
        {
            var companyPages = _companyService.GetCompanies(model.PageNumber, model.PageSize, model.KeyWords);

            ViewData["Keywords"] = model.KeyWords;
            ViewData["PageNumber"] = model.PageNumber;
            ViewData["TotalPages"] = companyPages.TotalPagesCount;

            List<Company> companies = companyPages.Items.ToList();
            return View("ViewCompanies", companies);
        }

        //[HttpGet]
        //public IActionResult DeleteCompany(long id)
        //{
        //    _companyService.DeleteCompany(id);

        //    TempData["Success"] = "Delete company successfully!";

        //    return RedirectToAction("ViewCompanies", "Company");
        //}

        [HttpGet]
        public async Task<IActionResult> CreateCompany()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCompany(CreateCompanyModel model)
        {
            var keys = ModelState.Keys;
            if (!ModelState.IsValid)
            {
                return View(nameof(CreateCompany), model);
            }

            if (model.MinScale > model.MaxScale)
            {
                ModelState.AddModelError("MaxScale", "Max Scale must be greater than Min Scale");
                return View(nameof(CreateCompany), model);
            }

            var company = new Company(

                );
            company.Name = model.Name;
            company.Phone = model.Phone;
            company.Email = model.Email;
            company.Address = model.Address;
            company.Longitude = model.Longitude;
            company.Latitude = model.Latitude;
            company.Country = model.Country;
            company.MinScale = model.MinScale;
            company.MaxScale = model.MaxScale;
            company.Description = model.Description;

            _companyService.CreateCompany(company);
            TempData["Success"] = "Create company successfully!";

            return RedirectToAction("ViewCompanies", "Company");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateCompany(long id)
        {
            var entity = await _companyService.GetById(id);

            var model = new UpdateCompanyModel();

            model.Id = entity.Id;
            model.Name = entity.Name;
            model.Phone = entity.Phone;
            model.Email = entity.Email;
            model.Address = entity.Address;
            model.Longitude = entity.Longitude;
            model.Latitude = entity.Latitude;
            model.Country = entity.Country;
            model.MinScale = entity.MinScale;
            model.MaxScale = entity.MaxScale;
            model.Description = entity.Description;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCompany(UpdateCompanyModel model)
        {
            var keys = ModelState.Keys;
            if (!ModelState.IsValid)
            {
                return View(nameof(UpdateCompany), model);
            }

            var company = _companyService.GetCompany(model.Id);
            company.Name = model.Name;
            company.Phone = model.Phone;
            company.Email = model.Email;
            company.Address = model.Address;
            company.Longitude = model.Longitude;
            company.Latitude = model.Latitude;
            company.Country = model.Country;
            company.MinScale = model.MinScale;
            company.MaxScale = model.MaxScale;
            company.Description = model.Description;


            _companyService.UpdateCompany(company);
            TempData["Success"] = "Update company successfully!";

            return RedirectToAction("ViewCompanies", "Company");
        }
    }
}
