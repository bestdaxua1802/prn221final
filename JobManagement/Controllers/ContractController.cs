﻿using Applications.Interfaces;
using Applications.Services;
using Domain.Entities;
using JobManagement.Models.Company;
using JobManagement.Models.Contract;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.Design;
using static Amazon.S3.Util.S3EventNotification;

namespace JobManagement.Controllers
{
    public class ContractController : Controller
    {
        private readonly IContractService _contractService;
        private readonly ICompanyService _companyService;
        private readonly IResumeService _resumeService;
        private readonly IJobDescriptionService _jobDescriptionService;
        

        public ContractController(
            IContractService contractService, 
            ICompanyService companyService, 
            IResumeService resumeService,
            IJobDescriptionService jobDescriptionService)
        {
            _contractService = contractService;
            _companyService = companyService;
            _resumeService = resumeService;
            _jobDescriptionService = jobDescriptionService;
        }

        public IActionResult Index()
        {
            var contracts = _contractService.GetAll();
            return View(contracts);
        }

        [HttpGet]
        public IActionResult ViewDetailContract(long id)
        {
            var contract = _contractService.GetContract(id);
            if (contract == null)
            {
                return NotFound();
            }
            return View(contract);
        }

        //[HttpGet]
        //public IActionResult ViewContracts(GetAllContractModel model)
        //{
        //    var contractPages = _contractService.GetContracts(model.PageNumber, model.PageSize, model.KeyWords);

        //    ViewData["Keywords"] = model.KeyWords;
        //    ViewData["PageNumber"] = model.PageNumber;
        //    ViewData["TotalPages"] = contractPages.TotalPagesCount;

        //    List<Contract> contracts = contractPages.Items.ToList();
        //    return View("ViewContracts", contracts);
        //}

        [HttpGet]
        public async Task<ActionResult> ViewContracts(GetAllContractModel model)
        {
            var contractPageResult = _contractService.GetContracts(model.PageSize, model.PageNumber, model.CompanyId);

            ViewData["TotalPages"] = contractPageResult.TotalPagesCount;
            ViewData["PageNumber"] = contractPageResult.PageIndex;

            ViewData["Companies"] = await _companyService.GetAll();
            ViewData["SelectedCompanyId"] = model.CompanyId;

            return View(contractPageResult.Items);
        }


        [HttpGet]
        public IActionResult DeleteContract(long id)
        {
            _contractService.DeleteContract(id);

            TempData["Success"] = "Delete contract successfully!";

            return RedirectToAction("ViewContracts", "Contract");
        }

        [HttpGet]
        public async Task<IActionResult> CreateContract(ViewContractModel model)
        {
            

            if (!ModelState.IsValid)
            {
                TempData["Error"] = "Create contract failed!";
                return RedirectToAction(nameof(ViewContracts));
            }
            
            var jobDes = await _jobDescriptionService.GetById(model.JobDescriptionId);

            if (jobDes == null || jobDes.Company.Id != model.CompanyId || jobDes.Resumes == null || !jobDes.Resumes.Any(resume => resume.Id == model.ResumeId))
            {
                TempData["Error"] = "Create contract failed!";
                return RedirectToAction(nameof(ViewContracts));
            }

            if (_contractService.Get(model.CompanyId, model.ResumeId) != null)
            {
                TempData["Error"] = "Resume has been had contract with this company!";
                return RedirectToAction(nameof(Index));
            }
            var contractModel = new CreateContractModel();
            contractModel.ResumeName = jobDes.Resumes.ToList().Find(resume => resume.Id == model.ResumeId).Name;
            contractModel.CompanyName = jobDes.Company.Name;
            contractModel.JobDescriptionId = jobDes.Id;
            return View(nameof(CreateContract), contractModel);
        }

        [HttpPost]
        public async Task<IActionResult> CreateContract(CreateContractModel model)
        {
            var keys = ModelState.Keys;
            if (!ModelState.IsValid)
            {
                return View(nameof(CreateContract), model);
            }

            var contract = new Contract(

                );
            contract.Interviewer = model.Interviewer;
            contract.InterviewTime = model.InterviewTime;
            contract.CompanyId = model.CompanyId;
            contract.ResumeId = model.ResumeId;
            contract.OfferSalary = model.OfferSalary;
            contract.RequestSalary = model.RequestSalary;
            contract.Description = model.Description;

            _contractService.CreateContract(contract);
            TempData["Success"] = "Create contract successfully!";

            return RedirectToAction("ViewContracts", "Contract");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateContract(long id)
        {
            var entity = await _contractService.GetById(id);

            var model = new UpdateContractModel();

            model.Id = entity.Id;
            model.Interviewer = entity.Interviewer;
            model.InterviewTime = entity.InterviewTime;
            model.CompanyId = entity.CompanyId;
            model.ResumeId = entity.ResumeId;
            model.OfferSalary = entity.OfferSalary;
            model.RequestSalary = entity.RequestSalary;
            model.Description = entity.Description;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateContract(UpdateContractModel model)
        {

            var contract = _contractService.GetContract(model.Id);
            contract.Interviewer = model.Interviewer;
            contract.InterviewTime = model.InterviewTime;
            contract.CompanyId = model.CompanyId;
            contract.ResumeId = model.ResumeId;
            contract.OfferSalary = model.OfferSalary;
            contract.RequestSalary = model.RequestSalary;
            contract.Description = model.Description;


            _contractService.UpdateContract(contract);
            TempData["Success"] = "Update contract successfully!";

            return RedirectToAction("ViewContracts", "Contract");
        }
    }
}
