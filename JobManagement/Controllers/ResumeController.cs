﻿using Applications.Interfaces;
using Applications.Models;
using Domain.Entities;
using JobManagement.Models.Resume;
using JobManagement.Services;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace JobManagement.Controllers
{
    public class ResumeController : Controller
    {
        private IResumeService _resumeService;
        private ISkillService _skillService;
        private IStorageService _storageService;

        public ResumeController(
            ISkillService skillService,
            IResumeService resumeService,
            IStorageService storageService)
        {
            _skillService = skillService;
            _resumeService = resumeService;
            _storageService = storageService;
        }


        public IActionResult Index()
        {
            List<Resume> resumes =  _resumeService.GetResumes();
            return View(resumes);
        }

        [HttpGet]
        public IActionResult ViewDetailResume(long id)
        {
            Console.WriteLine(id);
            var resume = _resumeService.GetResume(id);
            if (resume == null)
            {
                return NotFound();
            }
            Console.WriteLine(resume.Description);
            ViewData["Skills"] = resume.Skills;
            ViewData["FileUrl"] = _storageService.getFullPathFile(resume.FileUrl);
            return View(resume);
        }

        [HttpGet]
        public IActionResult DeleteResume(long id)
        {
            Console.WriteLine(id);
            _resumeService.DeleteResume(id);

            TempData["Success"] = "Delete resume successfully!";

            return RedirectToAction("ViewResumes", "Resume");
        }

        [HttpGet]
        public async Task<IActionResult> CreateResume()
        {
            ViewData["Skills"] = await _skillService.GetSkills();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateResume(CreateResumeModel model)
        {
            var keys = ModelState.Keys;
            if (!ModelState.IsValid)
            {
                ViewData["Skills"] = await _skillService.GetSkills();
                return View(nameof(CreateResume), model);
            }

            if (!model.File.FileName.Contains(".pdf"))
            {
                ViewData["Skills"] = await _skillService.GetSkills();
                ModelState.AddModelError("File", "Only accept PDF file");
                return View(nameof(CreateResume), model);
            }
            var resume = new Resume(

                );
            resume.Name = model.Name;
            resume.Phone = model.Phone;
            resume.Email = model.Email;
            resume.Birthday = model.Birthday;
            resume.Description = model.Description;

            var skills = new List<Skill>();
            foreach (var skillId in model.SkillIds)
            {
                Skill skill = await _skillService.GetDetail(skillId);
                if (skill == null)
                {
                    ModelState.AddModelError("SkillIds", "Skill not found");
                    ViewData["Skills"] = await _skillService.GetSkills();
                    return View(nameof(CreateResume), model);
                }
                skills.Add(skill);
            }

            resume.Skills = skills;

            await using var memoryStream = new MemoryStream();
            await model.File.CopyToAsync(memoryStream);

            var result = await _storageService.UploadFileAsync(model.File.FileName, memoryStream, "prn221");

            resume.FileUrl = "/" + model.File.FileName;

            _resumeService.CreateResume(resume);
            TempData["Success"] = "Create resume successfully!";

            return RedirectToAction("ViewResumes", "Resume");
        }

        [HttpGet]
        public async Task<IActionResult> UpdateResume(long id)
        {
            ViewData["Skills"] = await _skillService.GetSkills();
            var entity = _resumeService.GetResume(id);

            UpdateResumeModel model = new UpdateResumeModel();

            model.Id = entity.Id;
            model.Name = entity.Name;
            model.Phone = entity.Phone;
            model.Email = entity.Email;
            model.Birthday = entity.Birthday;
            model.Description = entity.Description;
            model.SkillIds = entity.Skills.Select(skill => skill.Id).ToList();
            model.FileName = entity.FileUrl.Substring(1);
            model.FileUrl = _storageService.getFullPathFile(entity.FileUrl);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateResume(UpdateResumeModel model)
        {

            if (!ModelState.IsValid)
            {

                ViewData["Skills"] = await _skillService.GetSkills();
                return View(nameof(UpdateResume), model);
            }

            if (model.UpdatedFile != null && !model.UpdatedFile.FileName.Contains(".pdf"))
            {
                ViewData["Skills"] = await _skillService.GetSkills();
                ModelState.AddModelError("UpdatedFile", "Only accept PDF file");
                return View(nameof(UpdateResume), model);
            }

            var resume = _resumeService.GetResume(model.Id);
            resume.Name = model.Name;
            resume.Phone = model.Phone;
            resume.Email = model.Email;
            resume.Birthday = model.Birthday;
            resume.Description = model.Description;

            var skills = new List<Skill>();
            foreach (var skillId in model.SkillIds)
            {
                Skill skill = await _skillService.GetDetail(skillId);
                if (skill == null)
                {
                    ModelState.AddModelError("SkillIds", "Skill not found");
                    return View(model);
                }
                skills.Add(skill);
            }

            resume.Skills = skills;

            await using var memoryStream = new MemoryStream();
            if (model.UpdatedFile != null)
            {
                await model.UpdatedFile.CopyToAsync(memoryStream);

                var result = await _storageService.UploadFileAsync(model.UpdatedFile.FileName, memoryStream, "prn221");

                resume.FileUrl = "/" + model.UpdatedFile.FileName;
            }

            _resumeService.UpdateResume(resume);
            TempData["Success"] = "Update resume successfully!";

            return RedirectToAction("ViewResumes", "Resume");
        }

        [HttpGet]
        public IActionResult ViewResumes(ViewResumesModel model)
        {
            var resumePages = _resumeService.GetResumes(model.PageNumber, model.PageSize, model.KeyWords);

            ViewData["Keywords"] = model.KeyWords;
            ViewData["PageNumber"] = model.PageNumber;
            ViewData["TotalPages"] = resumePages.TotalPagesCount;

            List<Resume> resumes = resumePages.Items.ToList();
            return View("ViewResumes", resumes);
        }



    }
}
