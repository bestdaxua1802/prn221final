﻿using System.ComponentModel.DataAnnotations;

namespace BusinessObject.Models
{
    internal class CapitalNameAttribute : ValidationAttribute 
    {
        public override bool IsValid(object? value)
        {
            if (value is string name)
            {
                foreach (var a in name.Trim().Split(" "))
                {
                    if (a[0] < 'A' || a[0] > 'Z') 
                    return false;
                }
            }
            return true;
        }
    }
}