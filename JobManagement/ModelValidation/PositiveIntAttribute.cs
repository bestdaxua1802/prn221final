﻿using System.ComponentModel.DataAnnotations;

namespace BusinessObject.Models
{
    internal class PositiveIntAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value is int number)
            {
                if (number > 0)
                    return true;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}