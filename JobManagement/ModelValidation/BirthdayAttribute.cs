﻿using System.ComponentModel.DataAnnotations;

namespace BusinessObject.Models
{
    internal class BirthdayAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if(value is DateTime birthday)
            {
                if (birthday.Date > DateTime.Parse("01/01/1901") && birthday.Date <= DateTime.Now) 
                    return true;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}