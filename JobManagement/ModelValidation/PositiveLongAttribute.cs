﻿using System.ComponentModel.DataAnnotations;

namespace BusinessObject.Models
{
    internal class PositiveLongAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value is long number)
            {
                if (number > 0)
                    return true;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}