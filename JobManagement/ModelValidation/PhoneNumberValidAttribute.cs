﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace JobManagement.ModelValidation
{
    public class PhoneNumberValidAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            Regex pattern = new Regex(@"(?<!\d)\d{10}(?!\d)");

            if (value is string phone)
            {
                if (pattern.IsMatch(phone))
                    return true;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}

