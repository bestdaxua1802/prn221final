﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace JobManagement.ModelValidation
{
    public class PassWordAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            Regex pattern = new Regex("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$");
            if (value is string passw)
            {
                if (pattern.IsMatch(passw))
                    return true;
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}

