﻿

using Applications.Interfaces;
using Applications.Repositories;
using Domain.Entities;
using Domain.EntitiesRelationship;

namespace Applications
{
    public interface IUnitOfWork
    {
        IGenericRepository<Application> ApplicationRepository { get; }
        IGenericRepository<Account> AccountRepository { get; }
        IJobDescriptionRepository JobDescriptionRepository { get; }
        IResumeRepository ResumeRepository { get; }
        IAccountRepository AccountRepositorys { get; }
        IGenericRepository<Skill> SkillReposiory { get; }
        IGenericRepository<JobDescriptionSkill> JobDescriptionSkillReposiory { get; }
        IGenericRepository<ResumeSkill> ResumeSkillReposiory { get; }
        ICompanyRepository CompanyRepository { get; }
        IContractRepository ContractRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
