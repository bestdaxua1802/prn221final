﻿using Applications.Commons;
using Applications.Models;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface IAccountService
    {
        AccountModel? GetById(long id);
        Account? GetByIdEntites(long id);
        Account? GetByUsernameLog(string username);
        AccountModel? GetByUsername(string username);
        bool Update(AccountModel model);
        List<Account> GetAccounts();
        Account? GetAccount(long id);
        void DeleteAccount(long id);
        void CreateAccount(Account account);
        Task<bool> UpdatePassword(long accountId, string pasword);
        void UpdateAccount(Account account);
        Pagination<Account> GetAccountPagin(string? query,
            AccountStatus? status,
            int pageNumber,
            int pageSize);
    }
}
