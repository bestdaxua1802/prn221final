﻿using Applications.Commons;
using Applications.Models;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface IResumeService
    {
        List<Resume> GetResumes();
        Task<List<ResumeModel>> GetResumes(List<long>? skillIds);
        Resume? GetResume(long id);
        void DeleteResume(long id);
        void CreateResume(Resume resume);
        void UpdateResume(Resume resume);
        Pagination<Resume> GetResumes(int pageSize, int pageNumber, string keyword);
    }
}
