﻿using Applications.Commons;
using Applications.Models;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface ICompanyService
    {
        Task<List<CompanyModel>> GetAll();
        Task<CompanyModel> GetById(long id);
        Company? GetCompany(long id);
        void DeleteCompany(long id);
        void CreateCompany(Company company);
        void UpdateCompany(Company company);
        Pagination<Company> GetCompanies(int pageSize, int pageNumber, string keyword);
    }
}
