﻿using Applications.Models;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface ISkillService
    {
        Task<SkillModel> GetById(long skillId);
        Task<Skill> GetDetail(long skillId);
        Task<List<SkillModel>> GetSkills();
    }
}
