﻿using Applications.Commons;
using Applications.Models;
using Domain.Entities;

namespace Applications.Interfaces
{
    public interface IContractService
    {
        Task<List<ContractModel>> GetAll();
        Task<ContractModel> GetById(long id);
        Task<ContractModel> Get(long companyId, long resumeId);
        Contract? GetContract(long id);
        void DeleteContract(long id);
        void CreateContract(Contract contract);
        void UpdateContract(Contract contract);
        Pagination<ContractModel> GetContracts(int pageSize, int pageNumber, long? companyId);
    }
}
