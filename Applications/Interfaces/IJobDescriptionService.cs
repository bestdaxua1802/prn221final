﻿using Applications.Commons;
using Applications.Models;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Interfaces
{
    public interface IJobDescriptionService
    {
        List<JobDescriptionModel> GetAll();
        Task<JobDescriptionModel?> GetById(long id);
        void Delete(long id);
        Task<bool> Create(JobDescriptionModel resume);
        Task<bool> AddResume(long jdId, long resumeId);
        Task<bool> RemoveResume(long jdId, long resumeId);
        Task<bool> Update(JobDescriptionModel resume);
        Task<Pagination<JobDescriptionModel>> GetAll(
            int pageSize, 
            int pageNumber, 
            string? query, 
            long? skillId, 
            long? companyId,
            JobLevel? level,
            JobType? type, bool includeExpired);
    }
}

