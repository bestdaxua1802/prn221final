﻿using Applications.Commons;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Repositories
{
    public interface IResumeRepository : IGenericRepository<Resume>
    {
        Task<Pagination<Resume>> ToPagination(int pageNumber = 0, int pageSize = 10, string keywords = "");
    }
}
