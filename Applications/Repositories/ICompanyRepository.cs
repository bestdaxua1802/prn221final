﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface ICompanyRepository : IGenericRepository<Company>
    {
        Task<Pagination<Company>> ToPaginationCompany(int pageNumber = 0, int pageSize = 10, string keywords = "");
    }
}
