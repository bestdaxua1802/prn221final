﻿using Applications.Commons;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface IContractRepository : IGenericRepository<Contract>
    {
        Task<Pagination<Contract>> ToPaginationContract(int pageNumber = 0, int pageSize = 10, long? companyId = null);
    }
}
