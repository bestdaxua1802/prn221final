﻿using Applications.Commons;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Repositories
{
    public interface IJobDescriptionRepository : IGenericRepository<JobDescription>
    {
        Task<Pagination<JobDescription>> ToPagination(int pageNumber, int pageSize, string? query, long? skillId,
            long? companyId, JobLevel? level,
            JobType? type, bool includeExpired);
    }
}
