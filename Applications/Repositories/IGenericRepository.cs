﻿using Applications.Commons;
using Domain.Base;
using System.Linq.Expressions;

namespace Applications.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        Task<List<TEntity>> GetEntitiesByIdsAsync(List<long?> Ids);
        Task<List<TEntity>> GetAllAsync();
        Task<IQueryable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        List<Expression<Func<TEntity, object>>> includes = null,
        bool disableTracking = true);
        Task DeleteAsync(TEntity entity);
        Task<TEntity?> GetByIdAsync(long? id);
        Task AddAsync(TEntity entity);
        void Update(TEntity entity);
        void Approve(TEntity entity);
        void UpdateRange(List<TEntity> entities);
        void SoftRemove(TEntity entity);
        Task AddRangeAsync(List<TEntity> entities);
        void SoftRemoveRange(List<TEntity> entities);
        Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> query();
        Task<Pagination<TEntity>> ToPagination(int pageNumber = 0, int pageSize = 10);
        IEnumerable<TEntity> GetAll();
        TEntity GetById(object? id);

        void Insert(TEntity obj);
        void Delete(object id);

        void Save();
    }
}
