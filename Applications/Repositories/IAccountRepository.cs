﻿using Domain.Enums;
using Domain.Entities;
using Applications.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Applications.Commons;

namespace Applications.Repositories
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        public Task<Pagination<Account>> GetAllAccount(
            AccountStatus? status,
            int pageNumber,
            int pageSize,
            string? query
            );
    }
}
