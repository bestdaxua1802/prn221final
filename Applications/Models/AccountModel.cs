﻿

using Domain.Entities;
using Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Applications.Models
{
    public class AccountModel
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Username { get; set; }
        public string? Password { get; set; }
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^(84|0[3|5|7|8|9])+([0-9]{8})$",
              ErrorMessage = "Invalid phone number")]
        public string Phone { get; set; }
        public string? Description { get; set; }

        public AccountStatus Status { get; set; }

        public bool? IsRootAdmin { get; set; } = false;

        public AccountModel()
        {

        }

        public AccountModel(Account account)
        {
            Id = account.Id;
            FullName = account.FullName;
            Username = account.Username;
            Phone = account.Phone;
            Status = account.Status;
            IsRootAdmin = account.IsRootAdmin;
            Description = account.Description;
            Email = account.Email;
            Password = account.Password;
        }
    }
}
