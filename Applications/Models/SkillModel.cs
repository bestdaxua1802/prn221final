﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Models
{
    public class SkillModel
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string? Description { get; set; }
        
    }
}
