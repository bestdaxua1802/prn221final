﻿using Domain.Entities;

namespace Applications.Models
{
    public class ContractModel
    {
        public long Id { get; set; }
        public DateTime InterviewTime { get; set; }

        public string? Interviewer { get; set; }

        public long? CompanyId { get; set; }
        public CompanyModel Company { get; set; }

        public long? ResumeId { get; set; }
        public ResumeModel Resume { get; set; }

        public Double OfferSalary { get; set; }    // Mức lương công ty offer

        public Double RequestSalary { get; set; }    // Mức lương ứng viên mong muốn
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ModifiedAt { get; set; }
    }
}
