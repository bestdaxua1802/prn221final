﻿using Domain.Entities;
using Domain.EntitiesRelationship;
using Domain.Enums;
using Org.BouncyCastle.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Models
{
    public class JobDescriptionModel
    {
        public long Id { get; set; }
        public string Title { get; set; }

        public string Description { get; set; }

        public int? RequiredNumber { get; set; }

        public long? MinSalary { get; set; }

        public long? MaxSalary { get; set; }

        public JobType Type { get; set; }

        public JobLevel Level { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public DateTime? ClosedAt { get; set; }

        public long CompanyId { get; set; }

        public CompanyModel Company { get; set; }

        public IList<SkillModel>? Skills { get; set; }

        public IList<ResumeModel>? Resumes { get; set; }
    }
}
