﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Models
{
    public class ResumeModel
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime Birthday { get; set; }

        public string FileUrl { get; set; }

        public string? Description { get; set; }

        public IList<Skill>? Skills { get; set; }
        public IList<JobDescription> JobDescriptions { get; set; }
    }
}
