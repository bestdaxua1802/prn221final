using Applications.Commons;
using Applications.Models;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
﻿using Applications.Interfaces;
using Applications.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AccountService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public AccountModel? GetById(long id)
        {
            var account = _unitOfWork.AccountRepository.Find(acc => acc.Id == id).Result;
            
            if (account.FirstOrDefault() != null)
            {
                return new AccountModel(account.FirstOrDefault());
            }
            return null;
        }
        public Account? GetByIdEntites(long id)
        {
            var account = _unitOfWork.AccountRepository.Find(acc => acc.Id == id).Result;

            if (account.FirstOrDefault() != null)
            {
                return account.FirstOrDefault();
            }
            return null;
        }

        public AccountModel? GetByUsername(string username)
        {
            var account = _unitOfWork.AccountRepository.Find(acc => acc.Username == username).Result;
            if (account.FirstOrDefault() != null)
            {
                return new AccountModel(account.FirstOrDefault());
            }
            return null;
        }
        public Account? GetByUsernameLog(string username)
        {
            if (username == null)
            {
                return null;
            }
            var account = _unitOfWork.AccountRepository.Find(acc => acc.Username == username).Result;
            if (account.FirstOrDefault() != null)
            {
                return account.FirstOrDefault();
            }
            return null;
        }

        public bool Update(AccountModel model)
        {
            var account = _unitOfWork.AccountRepository.Find(acc => acc.Id == model.Id).Result;

            if (account.FirstOrDefault() != null)
            {
                var validAccount = account.FirstOrDefault();
                validAccount.Email = model.Email;
                validAccount.FullName = model.FullName;
                validAccount.Description = model.Description;
                validAccount.Phone = model.Phone;

                _unitOfWork.AccountRepository.Update(validAccount);
                int result = _unitOfWork.SaveChangeAsync().Result;
                return true;
            }
            return false;
        }

        public async Task<bool> UpdatePassword(long accountId, string pasword)
        {
            var account = await _unitOfWork.AccountRepository.GetByIdAsync(accountId);

            if (account != null)
            {
                account.Password = pasword;

                _unitOfWork.AccountRepository.Update(account);
                await _unitOfWork.SaveChangeAsync();
                return true;
            }
            return false;
        }
        public Account? GetAccount(long id)
        {
            var account = _unitOfWork.AccountRepository.GetById(id);

            return account;
        }

        public void CreateAccount(Account account)
        {
            _unitOfWork.AccountRepository.AddAsync(account).Wait();
            int result = _unitOfWork.SaveChangeAsync().Result;
        }
        public void DeleteAccount(long id)
        {
            var account = _unitOfWork.AccountRepository.GetByIdAsync(id).Result;
            _unitOfWork.AccountRepository.SoftRemove(account);
            int result = _unitOfWork.SaveChangeAsync().Result;
        }
        public List<Account> GetAccounts()
        {
            var accounts =  _unitOfWork.AccountRepository.GetAllAsync().Result;
            return accounts;
        }

        public void UpdateAccount(Account account)
        {
            throw new NotImplementedException();
        }

        public Pagination<Account> GetAccountPagin(string? query,
            AccountStatus? status,
            int pageNumber,
            int pageSize)
        {
            var result = _unitOfWork.AccountRepositorys.GetAllAccount(status, pageNumber, pageSize, query).Result;
            return result;
        }
    }
}
