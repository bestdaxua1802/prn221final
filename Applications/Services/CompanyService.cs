﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Models;
using AutoMapper;
using Domain.Entities;
using Domain.EntitiesRelationship;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CompanyService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void CreateCompany(Company company)
        {
            _unitOfWork.CompanyRepository.AddAsync(company).Wait();
            int result = _unitOfWork.SaveChangeAsync().Result;
        }

        public void DeleteCompany(long id)
        {
            var company = _unitOfWork.CompanyRepository.GetByIdAsync(id).Result;
            _unitOfWork.CompanyRepository.SoftRemove(company);
            int result = _unitOfWork.SaveChangeAsync().Result;
        }

        public async Task<List<CompanyModel>> GetAll()
        {
            var companies = await _unitOfWork.CompanyRepository.GetAllAsync();
            var results = new List<CompanyModel>();
            foreach (var company in companies)
            {
                results.Add(_mapper.Map<CompanyModel>(company));
            }

            return results;
        }

        public async Task<CompanyModel> GetById(long id)
        {
            var company = await _unitOfWork.CompanyRepository.GetByIdAsync(id);
            return _mapper.Map<CompanyModel>(company);

        }

        public Pagination<Company> GetCompanies(int pageSize, int pageNumber, string keyword)
        {
            return _unitOfWork.CompanyRepository.ToPaginationCompany(pageSize, pageNumber, keyword).Result;
        }

        public Company? GetCompany(long id)
        {
            var companies = _unitOfWork.CompanyRepository.GetAsync(
                 predicate: x => x.Id == id,
                 includes: new List<Expression<Func<Company, object>>>()
                 {
                     company => company.Contracts,
                     company => company.JobDescriptions
                 }).Result;

            return companies.FirstOrDefault();
        }

        public void UpdateCompany(Company company)
        {
            _unitOfWork.CompanyRepository.Update(company);
            var result = _unitOfWork.SaveChangeAsync().Result;
        }
    }
}
