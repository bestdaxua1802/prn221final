﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Models;
using AutoMapper;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqKit;
using Domain.EntitiesRelationship;
using Applications.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Applications.Services
{
    public class JobDescriptionService : IJobDescriptionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public JobDescriptionService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> AddResume(long jdId, long resumeId)
        {
            Application application = new Application();
            application.JobDescriptionId = jdId;
            application.ResumeId = resumeId;
            await _unitOfWork.ApplicationRepository.AddAsync(application);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public async Task<bool> Create(JobDescriptionModel model)
        {
            var jobDescription = _mapper.Map<JobDescription>(model);
            jobDescription.CompanyId = model.Company.Id;
            jobDescription.Company = null;
            jobDescription.Skills = null;
            
            await _unitOfWork.JobDescriptionRepository.AddAsync(jobDescription);
            await _unitOfWork.SaveChangeAsync();

            var jdSkill = new List<JobDescriptionSkill>();
            foreach (var skill in model.Skills)
            {
                jdSkill.Add(new JobDescriptionSkill()
                {
                    JobDescriptionId = jobDescription.Id,
                    SkillId = skill.Id
                }) ;
            }
            _unitOfWork.JobDescriptionSkillReposiory.AddRangeAsync(jdSkill);
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public void Delete(long id)
        {
            var jobDescription = _unitOfWork.JobDescriptionRepository.GetByIdAsync(id).Result;
            _unitOfWork.JobDescriptionRepository.SoftRemove(jobDescription);
            var result = _unitOfWork.SaveChangeAsync().Result;
        }

        public List<JobDescriptionModel> GetAll()
        {
            var jobDescriptions = _unitOfWork.JobDescriptionRepository.GetAllAsync().Result;
            var results = new List<JobDescriptionModel>();
            foreach (var job in jobDescriptions)
            {
                results.Add(_mapper.Map<JobDescriptionModel>(job));
            }
            return results;
        }

        public async Task<Pagination<JobDescriptionModel>> GetAll(
            int pageSize, int pageNumber,
            string? query, long? skillId, 
            long? companyId, JobLevel? level, 
            JobType? type, bool includeExpired)
        {
            var jobDescriptions = await _unitOfWork.JobDescriptionRepository.ToPagination(pageNumber, pageSize,
                query, skillId, companyId, level, type, false);

            var results = new List<JobDescriptionModel>();
            foreach(var job in jobDescriptions.Items)
            {
                results.Add(_mapper.Map<JobDescriptionModel>(job));
            }

            return new Pagination<JobDescriptionModel>
            {
                Items = results,
                TotalItemsCount = jobDescriptions.TotalItemsCount,
                PageIndex = pageNumber,
                PageSize = pageSize,
            };
        }

        public async Task<JobDescriptionModel> GetById(long id)
        {
            var jobDescription = await _unitOfWork.JobDescriptionRepository.GetAsync(
                predicate: x => x.Id == id,
                includes: new List<Expression<Func<JobDescription, object>>>()
                {
                    jd => jd.Company,
                    jd => jd.Skills,
                    jd => jd.Resumes
                });
            return _mapper.Map<JobDescriptionModel>(jobDescription.FirstOrDefault());
        }

        public async Task<bool> RemoveResume(long jdId, long resumeId)
        {
            var resume = await _unitOfWork.ApplicationRepository.GetAsync(x => x.ResumeId == resumeId && x.JobDescriptionId == jdId);
            await _unitOfWork.ApplicationRepository.DeleteAsync(resume.FirstOrDefault());
            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        public async Task<bool> Update(JobDescriptionModel model)
        {
            var jobDescription = await _unitOfWork.JobDescriptionRepository.GetByIdAsync(model.Id);
            jobDescription = _mapper.Map<JobDescriptionModel, JobDescription>(model, jobDescription);
            jobDescription.Resumes = null;
            jobDescription.Skills = null;
            jobDescription.Company = null;
            jobDescription.CompanyId = model.CompanyId;
            _unitOfWork.JobDescriptionRepository.Update(jobDescription);
            await _unitOfWork.SaveChangeAsync();

            foreach (var skill in model.Skills)
            {
                var jobDescriptionSkills = _unitOfWork.JobDescriptionSkillReposiory.GetAsync(x => (x.SkillId == skill.Id) && (x.JobDescriptionId == model.Id)).Result;
                if (jobDescriptionSkills.FirstOrDefault() == null)
                {
                    JobDescriptionSkill jobDescriptionSkill = new JobDescriptionSkill();
                    jobDescriptionSkill.JobDescriptionId = model.Id;
                    jobDescriptionSkill.SkillId = skill.Id;
                    await _unitOfWork.JobDescriptionSkillReposiory.AddAsync(jobDescriptionSkill);
                }
            }

            await _unitOfWork.SaveChangeAsync();
            return true;
        }

        


    }
}
