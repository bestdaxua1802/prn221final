﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Models;
using AutoMapper;
using Domain.Entities;
using System.Data.Entity;
using System.Linq.Expressions;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace Applications.Services
{
    public class ContractService : IContractService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ContractService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public void CreateContract(Contract contract)
        {
            _unitOfWork.ContractRepository.AddAsync(contract);
            int result = _unitOfWork.SaveChangeAsync().Result;
        }

        public void DeleteContract(long id)
        {
            var contract = _unitOfWork.ContractRepository.GetByIdAsync(id).Result;
            _unitOfWork.ContractRepository.SoftRemove(contract);
            int result = _unitOfWork.SaveChangeAsync().Result;
        }

        public async Task<ContractModel?> Get(long companyId, long resumeId)
        {
            var contracts = await _unitOfWork.ContractRepository.GetAsync(
                predicate: con => con.CompanyId == companyId && con.ResumeId == resumeId
                );
            var contract = contracts.FirstOrDefault();
            if (contract == null )
            {
                return null;
            }
            return _mapper.Map<ContractModel?>(contract);
        }

        public async Task<List<ContractModel>> GetAll()
        {
            var contracts = await _unitOfWork.ContractRepository.GetAllAsync();
            var results = new List<ContractModel>();
            foreach (var contract in contracts)
            {
                results.Add(_mapper.Map<ContractModel>(contract));
            }

            return results;
        }

        public async Task<ContractModel> GetById(long id)
        {
            var contract = await _unitOfWork.ContractRepository.GetByIdAsync(id);
            return _mapper.Map<ContractModel>(contract);
        }

        public Contract? GetContract(long id)
        {
            var contracts = _unitOfWork.ContractRepository.GetAsync(
                 predicate: x => x.Id == id,
                 includes: new List<Expression<Func<Contract, object>>>()
                 {
                     contract => contract.Company,
                     contract => contract.Resume
                 }).Result;

            return contracts.FirstOrDefault();
        }

        public Pagination<ContractModel> GetContracts(int pageSize, int pageNumber, long? companyId)
        {
            List<Contract> contracts = null;
            //var contracts = _unitOfWork.ContractRepository.ToPaginationContract(pageSize, pageNumber, companyId).Result;
            if (companyId != null)
            {
                 contracts = _unitOfWork.ContractRepository.GetAsync(
                 predicate: x => x.CompanyId == companyId,
                 includes: new List<Expression<Func<Contract, object>>>()
                 {
                     contract => contract.Company,
                     contract => contract.Resume
                 }).Result.ToList();
            }
            else
            {
                contracts = _unitOfWork.ContractRepository.GetAsync(
                 includes: new List<Expression<Func<Contract, object>>>()
                 {
                     contract => contract.Company,
                     contract => contract.Resume
                 }).Result.ToList();
            }
            var contractPage = new Pagination<ContractModel>();
            contractPage.PageSize = pageSize;
            contractPage.PageIndex = pageNumber;

            var items = contracts.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            contractPage.TotalItemsCount = contracts.Count();
            contractPage.Items = new List<ContractModel>();
            foreach (var contract in items)
            {
                contractPage.Items.Add(_mapper.Map<ContractModel>(contract));
            }

            return contractPage;
        }

        public void UpdateContract(Contract contract)
        {
            _unitOfWork.ContractRepository.Update(contract);
            var result = _unitOfWork.SaveChangeAsync().Result;
        }
    }
}
