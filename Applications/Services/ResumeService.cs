﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Models;
using Applications.Repositories;
using AutoMapper;
using Domain.Entities;
using Domain.EntitiesRelationship;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;

namespace Applications.Services
{
    public class ResumeService : IResumeService
    {
        private IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        
        public ResumeService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void CreateResume(Resume resume)
        {
            _unitOfWork.ResumeRepository.AddAsync(resume).Wait();
            int result = _unitOfWork.SaveChangeAsync().Result;
        }

        public void DeleteResume(long id)
        {
            var resume = _unitOfWork.ResumeRepository.GetByIdAsync(id).Result;
            _unitOfWork.ResumeRepository.SoftRemove(resume);
            int result = _unitOfWork.SaveChangeAsync().Result;
        }

        public Resume? GetResume(long id)
        {
             var resumes = _unitOfWork.ResumeRepository.GetAsync(
                 predicate: x => x.Id == id,
                 includes: new List<Expression<Func<Resume, object>>>()
                 {
                     resume => resume.Skills
                 }).Result;
            
            return resumes.FirstOrDefault();
        }

        public List<Resume> GetResumes()
        {
            var resumes = _unitOfWork.ResumeRepository.GetAsync(
                predicate: resume => resume.IsDeleted == false
                ).Result;
            return resumes.ToList();
        }

        public Pagination<Resume> GetResumes(int pageSize, int pageNumber, string keyword)
        {
            return _unitOfWork.ResumeRepository.ToPagination(pageSize, pageNumber, keyword).Result;
        }

        public async Task<List<ResumeModel>> GetResumes(List<long>? skillIds)
        {
            if (skillIds != null && skillIds.Count() > 0)
            {
                var resumes = await _unitOfWork.ResumeRepository.GetAsync(
                    predicate: resume => resume.Skills.Any(skill => skillIds.Contains(skill.Id)),
                    includes: new List<Expression<Func<Resume, object>>>()
                    {
                        resume => resume.Skills,
                        resume => resume.JobDescriptions
                    });
                var results = new List<ResumeModel>();
                foreach (var resume in resumes)
                {
                    results.Add(_mapper.Map<ResumeModel>(resume));
                }
                return results;

            }
            return new List<ResumeModel>();
        }

        public void UpdateResume(Resume resume)
        {
            _unitOfWork.ResumeRepository.Update(resume);
            foreach (var skill in  resume.Skills)
            {
                var resumeSkills = _unitOfWork.ResumeSkillReposiory.GetAsync(x => (x.SkillId == skill.Id) && (x.ResumeId == resume.Id)).Result;
                if (resumeSkills.FirstOrDefault() == null)
                {
                    ResumeSkill resumeSkill = new ResumeSkill();
                    resumeSkill.ResumeId = resume.Id;
                    resumeSkill.SkillId = skill.Id;
                    bool isCompleted = _unitOfWork.ResumeSkillReposiory.AddAsync(resumeSkill).IsCompleted;
                }
            }

            var result = _unitOfWork.SaveChangeAsync().Result;
        }
    }
}
