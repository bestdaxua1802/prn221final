﻿using Applications.Interfaces;
using Applications.Models;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.Services
{
    public class SkillService : ISkillService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public SkillService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<SkillModel> GetById(long skillId)
        {
            Skill? result = _unitOfWork.SkillReposiory.GetByIdAsync(skillId).Result;
            if (result != null)
            {
                return _mapper.Map<SkillModel>(result);
            }
            return null;
        }

        public async Task<Skill> GetDetail(long skillId)
        {
            return await _unitOfWork.SkillReposiory.GetByIdAsync(skillId);
        }

        public async Task<List<SkillModel>> GetSkills()
        {
            var skills = await _unitOfWork.SkillReposiory.GetAllAsync();
            var results = new List<SkillModel>();
            foreach(var skill in skills)
            {
                results.Add(_mapper.Map<SkillModel>(skill));
            }
            return results;
        }
    }
}
