﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Enums
{
    public enum JobType
    {
        [Display(Name = "Part time")]
        PART_TIME,
        [Display(Name = "Full time")]
        FULL_TIME
    }
}
