﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Enums
{
    public enum JobLevel
    {
        [Display(Name = "Internship")]
        INTERNSHIP,
        [Display(Name = "Fresher")]
        FRESHER,
        [Display(Name = "Junior")]
        JUNIOR,
        [Display(Name = "Senior")]
        SENIOR
    }
}
