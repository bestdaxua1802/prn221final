﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Enums
{
    public enum AccountStatus
    {
        [Display(Name = "Active")]
        ACTIVE,
        [Display(Name = "Inactive")]
        INACTIVE
    }
}
