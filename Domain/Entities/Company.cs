﻿using Domain.Base;


namespace Domain.Entities
{
    public class Company : BaseEntity
    {
        public string Name { get; set; }    
        public string Email { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public float? Longitude { get; set; }

        public float? Latitude { get; set; }

        public string Country { get; set; }

        public int? MinScale { get; set; } // Số lượng nhân viên tối thiểu

        public int? MaxScale { get; set; }   // Số lượng nhân viên tối đa

        public string? Description { get; set; }

        public ICollection<JobDescription>? JobDescriptions { get; set; }
		public ICollection<Contract>? Contracts { get; set; }

	}
}
