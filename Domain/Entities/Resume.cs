﻿using Domain.Base;
using Domain.EntitiesRelationship;

namespace Domain.Entities
{
    public class Resume : BaseEntity
    { 

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public DateTime Birthday { get; set; }

        public string FileUrl { get; set; }

        public string? Description { get; set; } 

        public ICollection<Contract>? Contracts { get; set; }
        public IList<Skill> Skills { get; set; }

        public IList<JobDescription> JobDescriptions { get; set; }


    }
}
