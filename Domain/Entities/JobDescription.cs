﻿using Domain.Base;
using Domain.EntitiesRelationship;
using Domain.Enums;


namespace Domain.Entities
{
    public class JobDescription : BaseEntity
    {

        public string Title { get; set; }

        public string Description { get; set; }

        public int? RequiredNumber { get; set; }

        public long? MinSalary { get; set; }

        public long? MaxSalary { get; set; }

        public JobType Type { get; set; }

        public JobLevel Level { get; set; }

        public DateTime? ClosedAt { get; set; }

        public long CompanyId { get; set; }
        public Company Company { get; set; }

        public IList<Skill> Skills { get; set; }

        public IList<Resume> Resumes { get; set; }

    }
}
