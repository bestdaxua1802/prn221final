﻿using Domain.Base;
using Domain.EntitiesRelationship;

namespace Domain.Entities
{
    public class Skill : BaseEntity
    {
        public string Name { get; set; }    

        public string? Description { get; set; }

        public IList<JobDescription> JobDescriptions { get; set; }

        public IList<Resume> Resumes { get; set; }

    }
}
