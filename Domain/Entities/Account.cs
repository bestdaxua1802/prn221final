﻿using Domain.Enums;
using Domain.Base;

namespace Domain.Entities
{
    public class Account : BaseEntity
    {

        public string FullName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string? Email { get; set; }

        public string? Phone { get; set; }

        public string? Description { get; set; }

        public AccountStatus Status { get; set; }

        public bool? IsRootAdmin { get; set; } = false;
    }
}
