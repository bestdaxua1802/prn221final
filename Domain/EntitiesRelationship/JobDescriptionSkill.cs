﻿using Domain.Base;
using Domain.Entities;

namespace Domain.EntitiesRelationship
{
    public class JobDescriptionSkill : BaseEntity
	{
        public long JobDescriptionId { get; set; } 
        public JobDescription JobDescription { get; set; }

        public long SkillId { get; set; }
        public Skill Skill { get; set; }
    }
}
