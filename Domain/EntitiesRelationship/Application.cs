﻿using Domain.Base;
using Domain.Entities;

namespace Domain.EntitiesRelationship
{
	public class Application : BaseEntity
	{
		public long JobDescriptionId { get; set; }
		public JobDescription JobDescription { get; set; }
		public long ResumeId { get; set; }
		public Resume Resume { get; set; }
	}
}
