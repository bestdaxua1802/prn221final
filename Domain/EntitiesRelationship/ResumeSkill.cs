﻿using Domain.Base;
using Domain.Entities;

namespace Domain.EntitiesRelationship
{
    public class ResumeSkill : BaseEntity
	{
        public long ResumeId { get; set; }
        public Resume Resume { get; set; }

        public long SkillId { get; set; }
        public Skill Skill { get; set; }
    }
}
