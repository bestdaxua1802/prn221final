﻿
using Domain.Entities;
using Domain.EntitiesRelationship;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructures
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options) { }
		public DbSet<Account> Accounts { get; set; }
		public DbSet<Company> Companys { get; set; }
		public DbSet<Contract> Contracts { get; set; }
		public DbSet<JobDescription> JobDescriptions { get; set; }
		public DbSet<Resume> Resumes { get; set; }
		public DbSet<Skill> Skills { get; set; }
		public DbSet<Application> Application { get; set; }
		public DbSet<JobDescriptionSkill> JobDescriptionSkills { get; set; }
		public DbSet<ResumeSkill> ResumeSkills { get; set; }
		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }
    }
}
