﻿using Applications;
using Applications.Repositories;
using Domain.Entities;
using Domain.EntitiesRelationship;
using Infrastructures.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _appDBContext;
        private IGenericRepository<Account>? _accountRepository;
        private IJobDescriptionRepository? _jobDescriptionRepository;
        private IGenericRepository<Skill>? _skillRepository;
        private IGenericRepository<Application>? _applicationRepository;
        private IGenericRepository<Company>? _companyRepository;
        private IGenericRepository<ResumeSkill> _resumeSkillRepository;
        private IGenericRepository<JobDescriptionSkill>? _jobDescriptionSkillReposiory;
        private IResumeRepository? _resumeRepository;
        private IAccountRepository? _accountRepositorys;

        public UnitOfWork(AppDBContext appDBContext)
        {
            _appDBContext = appDBContext;
        }
        public IGenericRepository<Account> AccountRepository => _accountRepository ??= new GenericRepository<Account>(_appDBContext);

        public IJobDescriptionRepository JobDescriptionRepository => _jobDescriptionRepository ??= new JobDescriptionRepository(_appDBContext);

        public IGenericRepository<Skill> SkillReposiory => _skillRepository ??= new GenericRepository<Skill>(_appDBContext);

        public IResumeRepository ResumeRepository => _resumeRepository ??= new ResumeRepository(_appDBContext);
        public IAccountRepository AccountRepositorys => _accountRepositorys ??= new AccountRepository(_appDBContext);

        public IGenericRepository<ResumeSkill> ResumeSkillReposiory => _resumeSkillRepository ??= new GenericRepository<ResumeSkill>(_appDBContext);

        public IGenericRepository<JobDescriptionSkill> JobDescriptionSkillReposiory => _jobDescriptionSkillReposiory ??= new GenericRepository<JobDescriptionSkill>(_appDBContext);

        public IGenericRepository<Application> ApplicationRepository => _applicationRepository ??= new GenericRepository<Application>(_appDBContext);

        public ICompanyRepository CompanyRepository => new CompanyRepository(_appDBContext);

        public IContractRepository ContractRepository => new ContractRepository(_appDBContext);

        public async Task<int> SaveChangeAsync() => await _appDBContext.SaveChangesAsync();
    }
}
