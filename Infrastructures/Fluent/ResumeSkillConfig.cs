﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Domain.EntitiesRelationship;

namespace Infrastructures.Fluent
{
	public class ResumeSkillConfig : IEntityTypeConfiguration<ResumeSkill>
	{
		public void Configure(EntityTypeBuilder<ResumeSkill> builder)
		{
			builder.HasKey(k => new { k.ResumeId, k.SkillId });
			builder.HasIndex(x => x.Id).IsUnique();
			builder.Property(x => x.Id).UseIdentityColumn();

		}
	}
}
