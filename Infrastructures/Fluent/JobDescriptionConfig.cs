﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.EntitiesRelationship;
using System.Reflection.Emit;

namespace Infrastructures.Fluent
{
	public class JobDescriptionConfig : IEntityTypeConfiguration<JobDescription>
	{
		public void Configure(EntityTypeBuilder<JobDescription> builder)
        {
			builder.HasKey(x => x.Id);

            builder
             .HasMany(job => job.Skills)
             .WithMany(skill => skill.JobDescriptions)
             .UsingEntity<JobDescriptionSkill>(
                js => js.HasOne(prop => prop.Skill).WithMany().HasForeignKey(prop => prop.SkillId),
                js => js.HasOne(prop => prop.JobDescription).WithMany().HasForeignKey(prop => prop.JobDescriptionId),
                js => { });

            builder.HasMany(job => job.Resumes)
             .WithMany(resume => resume.JobDescriptions)
             .UsingEntity<Application>(
                js => js.HasOne(prop => prop.Resume).WithMany().HasForeignKey(prop => prop.ResumeId),
                js => js.HasOne(prop => prop.JobDescription).WithMany().HasForeignKey(prop => prop.JobDescriptionId),
                js => { });

            builder.HasOne<Company>(s => s.Company)
				.WithMany(s => s.JobDescriptions)
				.HasForeignKey(fk => fk.CompanyId);

		}
	}
}
