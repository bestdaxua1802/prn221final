﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.EntitiesRelationship;
using System.Reflection.Emit;

namespace Infrastructures.Fluent
{
	public class ResumeConfig : IEntityTypeConfiguration<Resume>
	{
		public void Configure(EntityTypeBuilder<Resume> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasMany(resume => resume.Skills)
                .WithMany(skill => skill.Resumes)
                .UsingEntity<ResumeSkill>(
                    js => js.HasOne(prop => prop.Skill).WithMany().HasForeignKey(prop => prop.SkillId),
                    js => js.HasOne(prop => prop.Resume).WithMany().HasForeignKey(prop => prop.ResumeId),
                    js => { }
                    );

        }
    }
}
