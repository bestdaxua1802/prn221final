﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.EntitiesRelationship;

namespace Infrastructures.Fluent
{
	public class ApplicationConfig : IEntityTypeConfiguration<Application>
	{
		public void Configure(EntityTypeBuilder<Application> builder)
		{
			builder.HasKey(k => new { k.JobDescriptionId, k.ResumeId });
			builder.HasIndex(x => x.Id).IsUnique();
			builder.Property(x => x.Id).UseIdentityColumn();

			
		}
	}
}
