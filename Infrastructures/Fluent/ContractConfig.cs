﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Fluent
{
	public class ContractConfig : IEntityTypeConfiguration<Contract>
	{
		public void Configure(EntityTypeBuilder<Contract> builder)
		{
			builder.HasKey(x => x.Id);

			builder.HasOne<Company>(s => s.Company)
				.WithMany(s => s.Contracts)
				.HasForeignKey(fk => fk.CompanyId);

			builder.HasOne<Resume>(s => s.Resume)
				.WithMany(s => s.Contracts)
				.HasForeignKey(fk => fk.ResumeId);
		}
	}
}
