﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class UpdateRelationShip : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobDescriptionSkill_JobDescriptions_JobDescriptionId",
                table: "JobDescriptionSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_JobDescriptionSkill_Skills_SkillId",
                table: "JobDescriptionSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_ResumeSkill_Resumes_ResumeId",
                table: "ResumeSkill");

            migrationBuilder.DropForeignKey(
                name: "FK_ResumeSkill_Skills_SkillId",
                table: "ResumeSkill");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ResumeSkill",
                table: "ResumeSkill");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobDescriptionSkill",
                table: "JobDescriptionSkill");

            migrationBuilder.RenameTable(
                name: "ResumeSkill",
                newName: "ResumeSkills");

            migrationBuilder.RenameTable(
                name: "JobDescriptionSkill",
                newName: "JobDescriptionSkills");

            migrationBuilder.RenameIndex(
                name: "IX_ResumeSkill_SkillId",
                table: "ResumeSkills",
                newName: "IX_ResumeSkills_SkillId");

            migrationBuilder.RenameIndex(
                name: "IX_ResumeSkill_Id",
                table: "ResumeSkills",
                newName: "IX_ResumeSkills_Id");

            migrationBuilder.RenameIndex(
                name: "IX_JobDescriptionSkill_SkillId",
                table: "JobDescriptionSkills",
                newName: "IX_JobDescriptionSkills_SkillId");

            migrationBuilder.RenameIndex(
                name: "IX_JobDescriptionSkill_Id",
                table: "JobDescriptionSkills",
                newName: "IX_JobDescriptionSkills_Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ResumeSkills",
                table: "ResumeSkills",
                columns: new[] { "ResumeId", "SkillId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobDescriptionSkills",
                table: "JobDescriptionSkills",
                columns: new[] { "JobDescriptionId", "SkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_JobDescriptionSkills_JobDescriptions_JobDescriptionId",
                table: "JobDescriptionSkills",
                column: "JobDescriptionId",
                principalTable: "JobDescriptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobDescriptionSkills_Skills_SkillId",
                table: "JobDescriptionSkills",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ResumeSkills_Resumes_ResumeId",
                table: "ResumeSkills",
                column: "ResumeId",
                principalTable: "Resumes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ResumeSkills_Skills_SkillId",
                table: "ResumeSkills",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_JobDescriptionSkills_JobDescriptions_JobDescriptionId",
                table: "JobDescriptionSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_JobDescriptionSkills_Skills_SkillId",
                table: "JobDescriptionSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_ResumeSkills_Resumes_ResumeId",
                table: "ResumeSkills");

            migrationBuilder.DropForeignKey(
                name: "FK_ResumeSkills_Skills_SkillId",
                table: "ResumeSkills");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ResumeSkills",
                table: "ResumeSkills");

            migrationBuilder.DropPrimaryKey(
                name: "PK_JobDescriptionSkills",
                table: "JobDescriptionSkills");

            migrationBuilder.RenameTable(
                name: "ResumeSkills",
                newName: "ResumeSkill");

            migrationBuilder.RenameTable(
                name: "JobDescriptionSkills",
                newName: "JobDescriptionSkill");

            migrationBuilder.RenameIndex(
                name: "IX_ResumeSkills_SkillId",
                table: "ResumeSkill",
                newName: "IX_ResumeSkill_SkillId");

            migrationBuilder.RenameIndex(
                name: "IX_ResumeSkills_Id",
                table: "ResumeSkill",
                newName: "IX_ResumeSkill_Id");

            migrationBuilder.RenameIndex(
                name: "IX_JobDescriptionSkills_SkillId",
                table: "JobDescriptionSkill",
                newName: "IX_JobDescriptionSkill_SkillId");

            migrationBuilder.RenameIndex(
                name: "IX_JobDescriptionSkills_Id",
                table: "JobDescriptionSkill",
                newName: "IX_JobDescriptionSkill_Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ResumeSkill",
                table: "ResumeSkill",
                columns: new[] { "ResumeId", "SkillId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_JobDescriptionSkill",
                table: "JobDescriptionSkill",
                columns: new[] { "JobDescriptionId", "SkillId" });

            migrationBuilder.AddForeignKey(
                name: "FK_JobDescriptionSkill_JobDescriptions_JobDescriptionId",
                table: "JobDescriptionSkill",
                column: "JobDescriptionId",
                principalTable: "JobDescriptions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_JobDescriptionSkill_Skills_SkillId",
                table: "JobDescriptionSkill",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ResumeSkill_Resumes_ResumeId",
                table: "ResumeSkill",
                column: "ResumeId",
                principalTable: "Resumes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ResumeSkill_Skills_SkillId",
                table: "ResumeSkill",
                column: "SkillId",
                principalTable: "Skills",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
