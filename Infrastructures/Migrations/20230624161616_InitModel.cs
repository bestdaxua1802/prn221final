﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class InitModel : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ResumeId1",
                table: "Application",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Application_ResumeId1",
                table: "Application",
                column: "ResumeId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Application_Resumes_ResumeId1",
                table: "Application",
                column: "ResumeId1",
                principalTable: "Resumes",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Application_Resumes_ResumeId1",
                table: "Application");

            migrationBuilder.DropIndex(
                name: "IX_Application_ResumeId1",
                table: "Application");

            migrationBuilder.DropColumn(
                name: "ResumeId1",
                table: "Application");
        }
    }
}
