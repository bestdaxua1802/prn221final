﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructures.Migrations
{
    /// <inheritdoc />
    public partial class FixEntitiesField : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "ResumeSkills",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "JobDescriptionSkills",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Contracts",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "Id",
                table: "Application",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.CreateIndex(
                name: "IX_ResumeSkills_Id",
                table: "ResumeSkills",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_JobDescriptionSkills_Id",
                table: "JobDescriptionSkills",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Application_Id",
                table: "Application",
                column: "Id",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ResumeSkills_Id",
                table: "ResumeSkills");

            migrationBuilder.DropIndex(
                name: "IX_JobDescriptionSkills_Id",
                table: "JobDescriptionSkills");

            migrationBuilder.DropIndex(
                name: "IX_Application_Id",
                table: "Application");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "ResumeSkills");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "JobDescriptionSkills");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Application");
        }
    }
}
