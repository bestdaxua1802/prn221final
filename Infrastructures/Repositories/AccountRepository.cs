﻿using Applications.Commons;
using Applications.Repositories;
using Applications.Utils;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;

namespace Infrastructures.Repositories
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        private readonly AppDBContext _dbContext;
        public AccountRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }

        public async Task<Pagination<Account>> GetAllAccount(
            AccountStatus? status,
            int pageNumber = 1,
            int pageSize = 10,
            string? query = ""
            )
        {
            var itemCount = await _dbSet.CountAsync();
            IQueryable<Account> tableQuery = this._dbSet
                 .Where(e => string.IsNullOrEmpty(query) ||
                 e.Username.ToLower().Contains(query.ToLower()) ||
                 e.FullName.ToLower().Contains(query.ToLower()) ||
                 (e.Email != null &&
                 e.Email.ToLower().Contains(query.ToLower())) ||
                 (e.Phone != null &&
                 e.Phone.ToLower().Contains(query.ToLower())))
                 .Where(e => status == null || e.Status == status)
                 .OrderByDescending(e => e.CreatedAt);

            var totalItems = tableQuery.Count();

            var itemresult = tableQuery.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize).ToList();

            var result = new Pagination<Account>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = itemresult,
            };
            return result;
        }
    }
}
