﻿using Applications.Commons;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ResumeRepository : GenericRepository<Resume>, IResumeRepository
    {
        public ResumeRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }

        public async Task<Pagination<Resume>> ToPagination(int pageNumber = 1, int pageSize = 10, string keywords = "")
        {
            var itemCount = await _dbSet.CountAsync();
            IQueryable<Resume> query;
            if (keywords != null && keywords.Trim().Length > 0)
            {
                query = this._dbSet
                .Where(res => res.Name.Contains(keywords) || res.Email.Contains(keywords) || res.Phone.Contains(keywords))
                .Where(res => res.IsDeleted == false)
                .OrderByDescending(resume => resume.CreatedAt);
            }
            else
            {
                query = this._dbSet
                .Where(res => res.IsDeleted == false)
                .OrderByDescending(resume => resume.CreatedAt);
            }


            int totalItems = query.Count();

            query = query.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .Include(resume => resume.Skills);


            var result = new Pagination<Resume>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = query.ToList(),
            };

            return result;
        }
    }
}
