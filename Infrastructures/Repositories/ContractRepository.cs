﻿using Applications.Commons;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class ContractRepository : GenericRepository<Contract>, IContractRepository
    {
        public ContractRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }

        public async Task<Pagination<Contract>> ToPaginationContract(int pageNumber = 0, int pageSize = 10, long? companyId = null)
        {
            var itemCount = await _dbSet.CountAsync();
            IQueryable<Contract> query;
            if (companyId != null)
            {
                query = this._dbSet
                .Where(x => x.CompanyId == companyId)
                .Include(con => con.Resume)
                .Include(con => con.Company)
                .OrderByDescending(x => x.CreatedAt);
            }
            else
            {
                query = this._dbSet
                .OrderByDescending(x => x.CreatedAt);
            }


            int totalItems = query.Count();

            query = query.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);


            var result = new Pagination<Contract>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = query.ToList(),
            };

            return result;
        }
    }
}
