﻿using Applications.Commons;
using Applications.Repositories;
using Domain.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Repository.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructures.Repositories
{
    public class JobDescriptionRepository : GenericRepository<JobDescription>, IJobDescriptionRepository
    {
        public JobDescriptionRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }

        public async Task<Pagination<JobDescription>> ToPagination(int pageNumber = 0, int pageSize = 10, string? query = null, long? skillId = null, long? companyId = null, JobLevel? level = null, JobType? type = null, bool includeExpired = true)
        {
            IQueryable<JobDescription> tableQuery = this._dbSet
                .Where(e => string.IsNullOrEmpty(query) || e.Title.ToLower().Contains(query.ToLower()))
                .Where(e => level == null || e.Level == level)
                .Where(e => type == null || e.Type == type)
                .Where(e => companyId == null || e.CompanyId == companyId)
                .Where(e => skillId == null || e.Skills.Any(s => s.Id == skillId))
                .Where(e => e.IsDeleted == false)
                .OrderByDescending(e => e.CreatedAt);

            if (!includeExpired)
            {
                tableQuery = tableQuery
                    .Where(e => e.IsDeleted == false)
                    .Where(e => e.ClosedAt == null || DateTime.Compare((DateTime)e.ClosedAt, DateTime.Now) >= 1);
            }

            var totalItems = tableQuery.Count();

            tableQuery = tableQuery.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .Include(x => x.Skills)
                .Include(x => x.Company);


            return new Pagination<JobDescription>
            {
                TotalItemsCount = totalItems,
                PageIndex = pageNumber,
                PageSize = pageSize,
                Items = tableQuery.ToList()
            };

        }
    }
}
