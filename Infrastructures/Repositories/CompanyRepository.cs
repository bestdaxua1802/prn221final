﻿using Applications.Commons;
using Applications.Repositories;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class CompanyRepository : GenericRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }

        public async Task<Pagination<Company>> ToPaginationCompany(int pageNumber = 0, int pageSize = 10, string keywords = "")
        {
            var itemCount = await _dbSet.CountAsync();
            IQueryable<Company> query;
            if (keywords != null && keywords.Trim().Length > 0)
            {
                query = this._dbSet
                .Where(x => x.Name.Contains(keywords) || x.Email.Contains(keywords) || x.Phone.Contains(keywords))
                .OrderByDescending(x => x.CreatedAt);
            }
            else
            {
                query = this._dbSet
                .OrderByDescending(x => x.CreatedAt);
            }


            int totalItems = query.Count();

            query = query.Skip((pageNumber - 1) * pageSize)
                .Take(pageSize);


            var result = new Pagination<Company>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = query.ToList(),
            };

            return result;
        }
    }
}
