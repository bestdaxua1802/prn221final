﻿using Applications.Commons;
using Applications.Repositories;
using Domain.Base;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructures.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext _dbContext;
        protected DbSet<TEntity> _dbSet;

        public GenericRepository(AppDBContext appDBContext)
            //ICurrentTime currentTime,
            //IClaimService claimService) // contructor 3 param
        {
            _dbContext = appDBContext;
            _dbSet = appDBContext.Set<TEntity>();
        }

        public async Task AddAsync(TEntity entity)
        {
            entity.CreatedAt = DateTime.UtcNow;
            await _dbSet.AddAsync(entity);
        }

        public async Task AddRangeAsync(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreatedAt = DateTime.UtcNow;
            }
            await _dbSet.AddRangeAsync(entities);
        }

        public Task<List<TEntity>> GetAllAsync() => _dbSet.ToListAsync();

        public async Task<TEntity?> GetByIdAsync(long? id)
        {
            var result = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }

        public void SoftRemove(TEntity entity)
        {
            entity.IsDeleted = true;
            _dbSet.Update(entity);
        }

        public Task<IQueryable<TEntity>> GetAsync(
        Expression<Func<TEntity, bool>> predicate = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
        List<Expression<Func<TEntity, object>>> includes = null,
        bool disableTracking = true)
        {
            IQueryable<TEntity> query = _dbSet;

            if (disableTracking) query = query.AsNoTracking();

            if (includes != null) query = includes.Aggregate(query, (current, include) => current.Include(include));

            if (predicate != null) query = query.Where(predicate);

            return Task.FromResult(orderBy != null ? orderBy(query).AsQueryable() : query.AsQueryable());
        }


        public void SoftRemoveRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
				entity.ModifiedAt = DateTime.UtcNow;
				entity.IsDeleted = true;
            }
            _dbSet.UpdateRange(entities);
        }

        public Task DeleteAsync(TEntity entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached) _dbContext.Set<TEntity>().Attach(entity);

            _dbSet.Remove(entity);

            return Task.CompletedTask;
        }


        public void Update(TEntity entity)
        {
            if (_dbContext.Entry(entity).State == EntityState.Detached) _dbContext.Set<TEntity>().Attach(entity);

            _dbContext.Entry(entity).State = EntityState.Modified;

            entity.ModifiedAt = DateTime.UtcNow;
            _dbContext.Set<TEntity>().Update(entity);
            //return Task.CompletedTask;

            //_context.Entry(obj).State = EntityState.Modified;
            //entity.ModifiedAt = DateTime.UtcNow;
            //_dbSet.Update(entity);

        }

        public void UpdateRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
				entity.ModifiedAt = DateTime.UtcNow;
			}
            _dbSet.UpdateRange(entities);
        }
        public  Task<List<TEntity>> Find(Expression<Func<TEntity, bool>> expression)
        {
            var data =  _dbSet.Where(expression).ToListAsync();
            return data;
        }
        public IQueryable<TEntity> query()
        {
            return _dbSet.AsQueryable();
        }

        public async Task<Pagination<TEntity>> ToPagination(int pageIndex = 0, int pageSize = 10)
        {
            var itemCount = await _dbSet.CountAsync();
            var items = await _dbSet.OrderByDescending(x => x.CreatedAt)
                                    .Skip(pageIndex * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
                                    .ToListAsync();

            var result = new Pagination<TEntity>()
            {
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        public void Approve(TEntity entity)
        {
            _dbSet.Update(entity);
        }

        public async Task<List<TEntity>> GetEntitiesByIdsAsync(List<long?> Ids) => await _dbSet.Where(x => Ids.Contains(x.Id)).ToListAsync();
        public TEntity GetById(object? id)
        {
            if (id == null)
            {
                return null;
            }

            return _dbSet.Find(id);
        }

        public void Insert(TEntity obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("Entity");
            }

            _dbSet.Add(obj);
            Save();
        }


        public void Delete(object id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("Id");
            }

            TEntity existing = _dbSet.Find(id);
            if (existing == null)
            {
                return;
            }

            _dbSet.Remove(existing);
            Save();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }
    }
}
