﻿using Applications;
using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Applications.Services;
using Infrastructures.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services, AppConfiguration appConfiguration, IConfiguration config)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IResumeService), typeof(ResumeService));
            services.AddScoped(typeof(ISkillService), typeof(SkillService));
            services.AddScoped(typeof(IJobDescriptionService), typeof(JobDescriptionService));
            services.AddScoped(typeof(ICompanyService), typeof(CompanyService));
            services.AddScoped(typeof(IResumeRepository), typeof(ResumeRepository));
            services.AddScoped(typeof(IAccountRepository), typeof(AccountRepository));
            services.AddScoped(typeof(IJobDescriptionRepository), typeof(JobDescriptionRepository));

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IJobDescriptionService, JobDescriptionService>();
            services.AddScoped<IContractService, ContractService>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddDbContext<AppDBContext>(op => op.UseSqlServer(config.GetConnectionString("DefaultConnection")));
            // Add Object Services

            return services;
        }
    }
}
