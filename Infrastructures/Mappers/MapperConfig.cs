using AutoMapper;
using Applications.Commons;
using Domain.Entities;
using Applications.Models;

namespace Infrastructures.Mappers
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            /* pagination */
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));

            CreateMap<JobDescription, JobDescriptionModel>();
            CreateMap<JobDescriptionModel, JobDescription>();

            CreateMap<CompanyModel, Company>();
            CreateMap<Company, CompanyModel>();

            CreateMap<ResumeModel, Resume>();
            CreateMap<Resume, ResumeModel>();

            CreateMap<SkillModel, Skill>();
            CreateMap<Skill, SkillModel>();

            CreateMap<Contract, ContractModel>();
            CreateMap<ContractModel, Contract>();
        }
    }
}
